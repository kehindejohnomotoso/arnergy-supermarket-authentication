const mongoose = require("mongoose");
const UserTypeAndRoles = new mongoose.Schema({
    userType:{
        type: String,
        required:true
    },
    userRoles:{
        type: Array,
        required:false
    }
},{
    collection:"UserTypeAndRoles"
})

module.exports = mongoose.model("UserTypeAndRoles",UserTypeAndRoles)