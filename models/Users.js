const mongoose = require("mongoose");
const crypto = require("crypto")
const Users = new mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    userType: {
        type: String,
    },
    dateCreated: {
        type: Date,
        required: true,
        default: Date.now
    },
    hash: String,
    salt: String
})

// Method to set salt and hash the password for a user
Users.methods.setPassword = function (password) {

    // Creating a unique salt for a particular user
    this.salt = crypto.randomBytes(16).toString('hex');

    // Hashing user's salt and password with 1000 iterations,
    this.hash = crypto.pbkdf2Sync(password, this.salt,
        1000, 64, `sha512`).toString(`hex`);
};

// Method to check the entered password is correct or not
Users.methods.validPassword = function (password) {
    var hash = crypto.pbkdf2Sync(password,
        this.salt, 1000, 64, `sha512`).toString(`hex`);
    return this.hash === hash;
};

module.exports = mongoose.model("Users", Users)