const express = require('express')
const router = express.Router()

const User = require("../models/Users")
const Roles = require("../models/UserTypeAndRoles")
//Login
router.post('/login', (req, res) => {
    let responseJson
    try {
        if(req.body.password.length < 1 || req.body.username.length < 1){
            return res.status(400).send({
                message: "Please provide username and password."
            }) 
        }
        const loginUser = User.findOne({username: req.body.username}, (err, user) => {
            if (user == null) {
                return res.status(400).send({
                    message: "User Account does not exist."
                })
            } else {
                if (user.validPassword(req.body.password)) {

                    const roles = Roles.findOne({userType: user.userType},(err,r)=>{
                        res.status(201).json({
                            message: "Login Successful",
                            user: {
                                username:user.username,
                                userType:user.userType,
                                roles: r.userRoles || []
                            },
                        })
                    });
                } else {
                    return res.status(400).send({
                        message: "Invalid Password."
                    })
                }
            }
        })
    } catch (err) {
        res.status(500).send({
            message: "Server down"
        })
    }
})
//Signup
router.post('/signup', (req, res) => {
    if(req.body.password.length < 1 || req.body.username.length < 1){
        return res.status(400).send({
            message: "Please provide username and password."
        }) 
    }
    else{
        const userTypeExist = Roles.findOne({userType: req.body.userType},(err,resp)=>{
            if(!resp){
                return res.status(400).send({
                    message:"User Type/Role does not exist. Available user roles are [SUPERVISOR,EMPLOYEE,CLIENT]"
                })
            }else{
                const isUserExist = User.findOne({username: req.body.username}, (err, user) => {
                    //Check if user already exist
                    if (user) {
                        return res.status(400).send({
                            message: "User Account Already Exist"
                        })
                    }else{
                        ///Create new account for user that doesnt exist
                        const reg = new User();
                        reg.username = req.body.username
                        reg.userType = req.body.userType
                        reg.setPassword(req.body.password)
                        reg.save((err, response) => {
                            if (err) {
                                res.status(400).send({
                                    message: "Registration Failed"
                                })
                            } else {
                                res.status(201).send({
                                    message: "Registration Successful"
                                })
                            }
                        })
                    }
                })
            }
        })
        
    }
})

module.exports = router