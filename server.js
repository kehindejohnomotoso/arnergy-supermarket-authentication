// require('dotenv').config()
const express = require('express')
const app = express()
const mongoose = require('mongoose')

mongoose.connect("mongodb+srv://arnergy:authentication@cluster0.uczjp.mongodb.net/supermarket?retryWrites=true&w=majority", {useNewUrlParser: true, useUnifiedTopology: true})
const db = mongoose.connection
db.on('error', (error) => console.error("Error connecting to database", error))
db.once('open', () => console.log("Database Connection Successful"))

app.use(express.json())

//Check if necessary default data exist in database for a first time run
const UserTypesAndRoles = require('./models/UserTypeAndRoles')
//UserTypesAndRoles.deleteOne({userType: 'CLIENT'},(err)=>{console.log("Delete Error",err)})
const checkRoles = async () => {
    const res = await UserTypesAndRoles.find();
    return res
}
checkRoles().then((utr) => {
    // console.log("UTR",utr)
    try {
        if(!utr.some(d => d.userType === "SUPERVISOR")){
            const SUPERVISOR = new UserTypesAndRoles()
            SUPERVISOR.userType = "SUPERVISOR";
            SUPERVISOR.userRoles = ["Manages product categories", "Manages products", "Manages Employees", "Manages Clients","Broadcasts Message"];
            SUPERVISOR.save((err,UserTR) =>{if(!err) {console.log("Supervisor Saved")} else {console.log("Error saving Supervisor",err)}})
        }
        if(!utr.some(d => d.userType === "EMPLOYEE")){
            const EMPLOYEE = new UserTypesAndRoles()
            EMPLOYEE.userType = "EMPLOYEE";
            EMPLOYEE.userRoles = ["Handle Payment","Enter Order"];
            EMPLOYEE.save((err,UserTR) =>{if(!err) {console.log("Employee Saved")} else {console.log("Error saving Employee",err)}})
        }
        if(!utr.some(d => d.userType === "CLIENT")){
            const CLIENT = new UserTypesAndRoles()
            CLIENT.userType = "CLIENT";
            CLIENT.userRoles = ["Checkout","Make Payment"];
            CLIENT.save((err,UserTR) =>{if(!err) {console.log("CLIENT Saved")} else {console.log("Error saving CLIENT",err)}})
        }
    } catch (err) {
        console.log("Error", err)
    }
})

const authenticationRouter = require("./routes/authentication")
app.use('/auth', authenticationRouter)

app.listen((process.env.PORT || 5000), () => console.log("Authentication Server Started"))

